#!/bin/bash

##########################################################
#
# Description : déploiement à la volée de conteneur docker
#
# Autheur : Yassine YADINE
#
# Date : 25-DEC-2023#
#
#########################################################
# VARIABLES section
#
create="--create"
drop="--drop"
info="--info"
start="--start"
stop="--stop"
ansible="--ansible"
user_name="yassine"
help=help
#
###########################################################
## Functions
help() {
	echo "

	Options : 
		- --create : lancer des conteneurs

		- --drop : supprimer les conteneurs crées par le script $0

		- --info : caractéristiques des conteneurs (ip, nom, user...)
		
		- --start : redémarrage des conteneurs

		- --stop : arrête les conteneurs lancés

		- --ansible : déploiement arborescence ansible
	
		- --help : pour obtenir de l'aide
	"
}

# CREATE option
if [ "$1" == "$create" ];
then

	echo " $create option choosen"
	nb_machine=1
	[ "$2" != "" ] && nb_machine=$2
	echo "number of machine to create $nb_machine"

	# Récupération de l'index courant
	currentIndex=`docker ps -a --format '{{ .Names}}' | grep yassine | awk -F "-" '{print $3}' | sort -r | head -1`
	echo " currentIndex: $currentIndex"

	startRange=1
	[ -z $currentIndex ] || startRange=$(($currentIndex + 1))
	echo "startRange -> $startRange"

	nb_max=$(($currentIndex + $nb_machine))
	echo "nb_max -> $nb_max"

	for i in $(seq $startRange $nb_max);
	do
		machine_os="alpine"
		machine_name="$user_name-$machine_os-$i"
		echo "machine name : $machine_name" 
		docker run -tid --name $machine_name alpine:latest
		
	done

# DROP option
elif [ "$1" == "$drop" ];
then
	echo " $drop option choosen"

	docker rm -f $(docker ps -a | grep $user_name | awk '{print $1}')


# INFOS option
elif [ "$1" == "$info" ];
then
	echo " $info option choosen"
	for conteneurId in $(docker ps -a | grep $user_name | awk '{print $1}')
	do
		docker inspect -f ' => {{.Name}} - {{.State.Status}} at {{.NetworkSettings.IPAddress}}' $conteneurId
	done


# START option 
elif [ "$1" == "$start" ];
then
	echo " $start option choosen"
	docker start $(docker ps -a | grep $user_name | awk '{print $1}')

# STOP option 
elif [ "$1" == "$stop" ];
then
	echo " $stop option choosen"
	docker stop $(docker ps -a | grep $user_name | awk '{print $1}')

# ANSIBLE option 
elif [ "$1" == "$ansible" ];
then
	echo " $ansible option choosen"

# HELP option
elif [ "$1" == "$help" ];
then
	echo " $help option choosen"
	help

else
	help

fi

