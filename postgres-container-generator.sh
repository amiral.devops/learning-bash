#!/bin/bash

########################################
#
# Generateur de docker-compose
#
########################################

## Variables ###########################

#DIR="${HOME}/Projects/generator"
DIR="${PWD}"

## Functions ###########################

help() {	
	echo "USAGE :
	${0##*/} [-h] [--help]

	Options :

	-h, --help : aides

	-p, --postgre : lance une instance Postgres
"
}

parse_options() {
	case $@ in
		-h|--help)
			help
			;;
		-p|--postgres)
			install_postgres
			;;
		*)
		echo "option invalide, lancez -h ou --help"
	esac
}

install_postgres() {
echo "1- Installation d'une instance postgres"
echo "Création du répertoire de datas"
mkdir -p $DIR
cd $DIR && mkdir postgres
echo "
version: '3.7'
services:
  postgres:
    image: postgres:latest
    container_name: yassine-postgres
    environment:
    - POSTGRES_USER=yassine
    - POSTGRES_PASSWORD=yassine_pwd
    - POSTGRES_DB=mydb
    ports:
    - 5432:5432
    volumes:
    - postgres_data:/var/lib/postgres
    networks:
    - generator_net
volumes:
  postgres_data:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: ${DIR}/postgres
networks:
  generator_net:
    driver: bridge
    ipam:
      config:
        - subnet: 192.168.56.0/24

" > $DIR/docker-compose-postgres.yml

echo "2 - Run de l'instance..."
docker-compose -f $DIR/docker-compose-postgres.yml up -d

echo "\n Credentials :
    - POSTGRES_USER: yassine
    - POSTGRES_PASSWORD: yassine_pwd
    - POSTGRES_DB: mydb
    - PORT: 5432 \n

Command : psql -h <ip> -u yassine -d mydb
"


}
## Execute ###########################
parse_options $@
